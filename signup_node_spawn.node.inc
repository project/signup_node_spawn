<?php


/**
 * Node form callback. Build signup_node_spawn_node settings form. 
 */
function signup_node_spawn_node_settings($form_state) {

  $form = array();

  $nid = arg(1);
  $node = node_load($nid);

  // Get default spawn value
  $nid = $form['#node']->nid;
  $default_spawn_value = $node->signup_node_spawn;

  $form['signup_node_spawn_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Signup spawning'),
    '#collapsible' => TRUE,
    '#description' => t("Signup spawning allows you to automate the copy-based creation of new signup nodes based on existing nodes. If you turn spawning on for this node, when signups close, a new copy of this node will be created, including the spawn settings. Spawning will be turned off on the original. This allows for continuous ongoing creation of signup nodes when seating limits close. You can turn off spawning at any time."),
  );
  $form['signup_node_spawn_settings']['signup_node_spawn'] = array(
    '#type' => 'checkbox',
    '#title' => t('Spawn a new copy of this node when signups close?'),
    '#default_value' => $default_spawn_value,
    '#description' => t("Spawning is only recommended with signups that close due to seat limits being met. To avoid runaway spawns, ensure any date and scheduler fields integrated with Signup on this node are left blank. If you are manually closing signups on a node, you'll want to ensure that spawning is turned off prior to doing so, unless you want to spawn a new node automatically."),
  );  

  //$form['hidden'] = array('#type' => 'value', '#value' => array('node' => $node));
  $form['#node'] = $node;
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Custom submit function for use with override of signup_node_settings form.
 *
 */
function signup_node_spawn_node_settings_submit($form, &$form_state) {

  $nid = $form['#node']->nid;
  $spawn = $form_state['values']['signup_node_spawn'];

  $args = array('nid' => $nid, 'spawn' => $spawn);

  $exists = db_result(db_query("SELECT nid FROM {signup} WHERE nid = %d", $nid));
  if ($exists) { // Update
    drupal_write_record('signup', $args, 'nid');
  }
  else{ // Insert
    drupal_write_record('signup', $args);
  }

  drupal_set_message('Signup spawning settings saved.');

  return t('Saved.');
}
