DESCRIPTION
--------------------------
Signup Node Spawn allows you to automate the copy-based creation of new signup
nodes based on existing nodes. If you turn spawning on for a signup node, when
signups close, a new copy of the node will be created, including the spawn
settings. Spawning will be turned off on the original. This allows for
continuous ongoing creation of signup nodes when seating limits close. You can
turn off spawning at any time.

INSTALLATION
--------------------------
Install the module as usual, see http://drupal.org/node/70151 for further 
information.

Note this, from the Signup module:
Signups close in 3 main cases: (1) it is a time-based node and the
close-in-advance time has been reached (auto-close via cron); (2) the node
has a signup limit and the limit is reached; (3) a signup administrator
manually closes signups.

IMPORTANT:
Spawning is only recommended with signups that close due to seat limits being
met (2). To avoid runaway spawns, ensure any CCK dates integrated with Signup on
this node (1) are left blank. If you are manually closing signups on a node (3), 
you'll want to ensure that spawning is turned off prior to doing so, unless you 
want to spawn a new node automatically. 
 
PERMISSIONS
--------------------------
Signup Node Spawn piggybacks off of Signup's permissions system. In order to 
access the admin UI, the user should have "administer all signups" permission.
To access a node's Signup Spawning tab, the user should have "administer all
signups" or "administer signups for own content" (and they should be the owner
of that node).
 
MODULES
--------------------------
Ships with just Signup Node Spawn.
Signup is a dependency.
Plays nice with other Signup modules.

PAGES
--------------------------
admin/setings/signup_node_spawn - Admin UI
node/123/node/signups/spawn - Signup Spawning settings tab for node with ID 123

TABLES
--------------------------
Adds a "spawn" column to {signup} for a node's signup spawn settings.

BLOCKS
--------------------------
None.

HOOKS
--------------------------
See the API section in signup_node_spawn.module.

CREDITS
--------------------------
Signup Node Spawn was created by Michael Samuelson <mlsamuelson@gmail.com>
(http://mlsamuelson.com).
