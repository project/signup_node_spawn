<?php


/**
 * Admin form callback. Build signup_node_spawn settings form. 
 */
function signup_node_spawn_admin_settings() {

  $intro = 'Signup Node Spawn allows you to mark a signup node for respawning ';
  $intro .= '- that is, once the signups for the node close, a new instance of';
  $intro .= ' the node "spawns." It is recommended that the configured Signup';
  $intro .= ' date CCK field NOT be required, otherwise, if a signup node ';
  $intro .= 'is configured with a start and end date and it closes, the node ';
  $intro .= 'will respawn with the same date, triggering another close and ';
  $intro .= 'spawning, etc...';
  $intro = t($intro);

  $form['signup_node_spawn_intro'] = array(
    '#type' => 'markup',
    '#value' => $intro,
  );

  // Get signup content types for options.
  $signup_content_type_options = signup_node_spawn_get_signup_content_type_options();

  $form['signup_node_spawn_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Signup-enabled content types to provide spawning options on'),
    '#default_value' => variable_get('signup_node_spawn_content_types', array()),
    '#options' => $signup_content_type_options,
    '#description' => t('Content types must be signup-enabled to appear here.'),
  );

  return system_settings_form($form);
}

